import 'package:flutter/material.dart';

class BoxAnimatePage extends StatelessWidget {
  const BoxAnimatePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(
      child: _BoxAnimate(),
    ));
  }
}

class _BoxAnimate extends StatefulWidget {
  const _BoxAnimate({Key? key}) : super(key: key);

  @override
  State<_BoxAnimate> createState() => _BoxAnimateState();
}

class _BoxAnimateState extends State<_BoxAnimate> with SingleTickerProviderStateMixin {

  late AnimationController controller;
  //Animations
  late Animation<double> moveRight;
  late Animation<double> moveUp;
  late Animation<double> moveLeft;
  late Animation<double> moveDown;


  @override
  void initState() {
    super.initState();

    controller = AnimationController(vsync: this, duration: Duration(milliseconds:  4500));
    moveRight = Tween(begin: 0.0, end: 100.0).animate(
      CurvedAnimation(parent: controller, curve: Interval(0, 0.25, curve: Curves.bounceOut))
    );

    moveUp = Tween(begin: 0.0, end: -100.0).animate(
        CurvedAnimation(parent: controller, curve: Interval(0.25, 0.5, curve: Curves.bounceOut))
    );

    moveLeft = Tween(begin: 0.0, end: -100.0).animate(
        CurvedAnimation(parent: controller, curve: Interval(0.5, 0.75, curve: Curves.bounceOut))
    );

    moveDown = Tween(begin: 0.0, end: 100.0).animate(
        CurvedAnimation(parent: controller, curve: Interval(0.75, 1.0, curve: Curves.bounceOut))
    );

    controller.addListener(() { if(controller.status == AnimationStatus.completed){
      controller.reset();}
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    controller.forward();
    return AnimatedBuilder(
        animation: controller,
        child: _RectAngle(),
        builder: (BuildContext context, Widget? child) {
          return Transform.translate(
            offset: Offset(moveRight.value +  moveLeft.value, moveUp.value + moveDown.value),
            child: child,
          );
        },

    );
  }
}


class _RectAngle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      height: 70,
      decoration: BoxDecoration(color: Colors.blue),
    );
  }
}
