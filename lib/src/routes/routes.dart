import 'package:diseno_flutter/src/pages/pinterest_pages.dart';
import 'package:diseno_flutter/src/pages/slider_list_page.dart';
import 'package:flutter/material.dart';
import 'package:diseno_flutter/src/pages/slideshow_page.dart';
import 'package:diseno_flutter/src/pages/emergency_page.dart';
import 'package:diseno_flutter/src/pages/graphy_circle_page.dart';
import 'package:diseno_flutter/src/pages/headers_page.dart';
import 'package:diseno_flutter/src/retos/box_animate.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

final pageRoutes = <_Route>[
  _Route(FontAwesomeIcons.slideshare, 'SlideShow', SlideShowPage()),
  _Route(FontAwesomeIcons.truckMedical, 'Emergency', EmergencyPage()),
  _Route(FontAwesomeIcons.heading, 'Headers', HeaderPage()),
  _Route(FontAwesomeIcons.peopleCarryBox, 'Box Animate', BoxAnimatePage()),
  _Route(FontAwesomeIcons.circleNotch, 'Bar Progress', GraphyCirclePage()),
  _Route(FontAwesomeIcons.pinterest, 'Pinterest', PinterestPage()),
  _Route(FontAwesomeIcons.mobile, 'Slivers', SliderListPage()),

];

class _Route{
  final IconData icon;
  final String title;
  final Widget page;

  _Route(this.icon, this.title, this.page);
}
