import  'package:flutter/material.dart';
import 'dart:math' as Math;
class AnimationPages extends StatelessWidget {
  const AnimationPages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BoxAnimate(),
      )
    );
  }
}

class BoxAnimate extends StatefulWidget {
  @override
  State<BoxAnimate> createState() => _BoxAnimateState();
}

class _BoxAnimateState extends State<BoxAnimate> with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> rotation;
  late Animation<double> opacity;
  late Animation<double> opacityOut;
  late Animation<double> movieRight;
  late Animation<double> sizeScale;

  @override
  void initState() {
   controller =  AnimationController(vsync: this, duration: Duration(milliseconds: 4000) );

   rotation = Tween(begin: 0.0, end: 4 * Math.pi).animate(
     CurvedAnimation(parent: controller, curve: Curves.easeOut)
   );

   opacity = Tween(begin:  0.1, end: 1.0).animate(
     CurvedAnimation(parent: controller, curve: Interval(0, 0.25, curve: Curves.easeInOut))
   );

   opacityOut = Tween(begin:  0.0, end: 1.0).animate(
       CurvedAnimation(parent: controller, curve: Interval(0.75, 1.0, curve: Curves.easeInOut))
   );

   movieRight = Tween(begin: 0.0, end: 200.0).animate(
     CurvedAnimation(parent: controller, curve: Curves.easeOut)
   );

   sizeScale = Tween(begin: 0.0, end: 2.0).animate(
       CurvedAnimation(parent: controller, curve: Curves.easeOut)
   );

   controller.addListener(() {
     if(controller.status == AnimationStatus.completed){
       //controller.reverse();
       //controller.repeat();
       controller.reset();
     }else if(controller.status == AnimationStatus.dismissed){
       //controller.forward();
     }
   });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //Play
    controller.forward();

    return AnimatedBuilder(
        animation: controller,
        child: _Rectangulo(),
        builder: (BuildContext context, Widget? childRect){
          return Transform.translate(
              offset: Offset(movieRight.value , 0),
              child: Transform.rotate(
                angle: rotation.value,
                child: Opacity(
                    opacity: opacity.value - opacityOut.value,
                    child: Transform.scale(
                      scale: sizeScale.value,
                      child: childRect,
                    )),
              ));
        } );
  }
}



class _Rectangulo extends StatelessWidget {

  Widget build(BuildContext context) {
    return Container(
      width: 70,
      height: 70,
      decoration: BoxDecoration(
        color: Colors.blue
      ),
    );
  }
}
