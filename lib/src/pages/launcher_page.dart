import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:diseno_flutter/src/routes/routes.dart';
import 'package:diseno_flutter/src/theme/themeChange.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class LauncherPage extends StatelessWidget {
  const LauncherPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Design Flutter'),
      ),
      drawer: _FirstMenu(),
      body: Center(
        child: _ListOptions()
      )
    );
  }
}


class _ListOptions extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChange>(context).currentTheme;
    return ListView.separated(
      physics: BouncingScrollPhysics(),
        separatorBuilder: (context, i) => Divider(
          color: appTheme.primaryColorLight,
        ),
        itemCount: pageRoutes.length,
        itemBuilder: (context, i) => ListTile(
          leading: FaIcon(pageRoutes[i].icon, color: appTheme.colorScheme.secondary),
          title: Text(pageRoutes[i].title),
          trailing: Icon(Icons.chevron_right, color:  appTheme.colorScheme.secondary),
          onTap:(){
            Navigator.push(context, MaterialPageRoute(builder: (context) => pageRoutes[i].page));
          }
        ),
    );
  }
}

class _FirstMenu extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChange>(context);
    return Drawer(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(20),
              width:  double.infinity,
              height: 200,
              margin: const EdgeInsets.only(top: 10.0),
              child: CircleAvatar(
                backgroundColor: appTheme.currentTheme.colorScheme.secondary,
                child: Text('FH', style: TextStyle(fontSize: 50)),
              ),
            ),
            Expanded(child: _ListOptions()),

            ListTile(
              leading: Icon(Icons.lightbulb_outline, color: appTheme.currentTheme.colorScheme.secondary),
              title: Text('Dark Mode'),
              trailing: Switch.adaptive(
                  value: appTheme.darkTheme,
                  activeColor: appTheme.currentTheme.colorScheme.secondary,
                  onChanged: (value) => appTheme.darkTheme =  value

              ),

            ),

            SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: ListTile(
                leading: Icon(Icons.add_to_home_screen, color: appTheme.currentTheme.colorScheme.secondary,),
                title: Text('Custom Theme'),
                trailing: Switch.adaptive(
                    value: appTheme.customTheme,
                    activeColor:  appTheme.currentTheme.colorScheme.secondary,
                    onChanged: (value) => appTheme.customTheme =  value
                ),

              ),
            ),
          ],
        ),
      ),
    );
  }
}
