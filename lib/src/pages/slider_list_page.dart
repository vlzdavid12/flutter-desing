import 'package:diseno_flutter/src/theme/themeChange.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SliderListPage extends StatelessWidget {
  const SliderListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _MainScroll(),
          Positioned(bottom: -10, right: 0, child: const _NewButtonList())
        ],
      ),
    );
  }
}

class _MainScroll extends StatelessWidget {
  final items = [
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
  ];

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChange>(context).currentTheme;
    return CustomScrollView(
      slivers: <Widget>[
        /* const SliverAppBar(
          floating: true,
          elevation: 0,
          centerTitle: true,
          backgroundColor:  Colors.red,
          title: Text('Hola Mundo'),
        ),*/
        SliverPersistentHeader(
            floating: true,
            delegate: _SliverCustomHeaderDelegate(
                minHeight: 170,
                maxHeight: 190,
                child: Container(
                  alignment: Alignment.centerLeft,
                  color: appTheme.scaffoldBackgroundColor,
                  child: _Title(),
                ))),
        SliverList(
            delegate: SliverChildListDelegate(
                [...items, const SizedBox(height: 100)]))
      ],
    );
  }
}

class _SliverCustomHeaderDelegate extends SliverPersistentHeaderDelegate {
  final double minHeight;
  final double maxHeight;
  final Widget child;

  _SliverCustomHeaderDelegate(
      {required this.minHeight, required this.maxHeight, required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return SizedBox.expand(child: child);
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => maxHeight;

  @override
  // TODO: implement minExtent
  double get minExtent => minHeight;

  @override
  bool shouldRebuild(_SliverCustomHeaderDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}

class _Title extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChange>(context);

    return Column(
      children: <Widget>[
        SizedBox(height: 30),
        Container(
            margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: Text('New',
                style: TextStyle(
    color: (appTheme.darkTheme)? Colors.grey : Color(0xff532128),
    fontSize: 50)
    )),
        Stack(
          children: <Widget>[
            SizedBox(
              width: 100,
            ),
            Positioned(
              bottom: 8,
              child: Container(
                  width: 120,
                  height: 8,
                  color: (appTheme.darkTheme)? Colors.grey : Color(0XFFf7CDD5)
              ),
            ),
            Container(
                child: Text('List',
                    style: TextStyle(
                        color: Color(0xffD93A30),
                        fontSize: 50,
                        fontWeight: FontWeight.bold))),
          ],
        )
      ],
    );
  }
}

class _ListTask extends StatelessWidget {
  final items = [
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) => items[index]);
  }
}

class _ListItem extends StatelessWidget {
  final String title;
  final Color color;

  const _ListItem(this.title, this.color);

  @override
  Widget build(BuildContext context) {
    final appTheme =  Provider.of<ThemeChange>(context);
    return Container(
      alignment: Alignment.centerLeft,
      child: Text(this.title,
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20)
      ),
      padding: EdgeInsets.all(30),
      height: 110,
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: (appTheme.darkTheme)? Colors.deepOrangeAccent : this.color,
          borderRadius: BorderRadius.circular(30)
      ),
    );
  }
}

class _NewButtonList extends StatelessWidget {
  const _NewButtonList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final appTheme = Provider.of<ThemeChange>(context);

    return ButtonTheme(
        minWidth: size.width * 0.9,
        height: 100,
        child: RaisedButton(
          child: Text('Create new list',
              style: TextStyle(
                  color: (appTheme.darkTheme)
                      ? appTheme.currentTheme.scaffoldBackgroundColor
                      : Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 3)),
          color: (appTheme.darkTheme)
              ? appTheme.currentTheme.colorScheme.secondary
              : Color(0xffEd6762),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(topLeft: Radius.circular(50))),
          onPressed: () {},
        ));
  }
}
