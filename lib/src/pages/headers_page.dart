import 'package:diseno_flutter/src/theme/themeChange.dart';
import 'package:flutter/material.dart';
import 'package:diseno_flutter/src/widgets/header.dart';
import 'package:provider/provider.dart';

class HeaderPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final accentColor = Provider.of<ThemeChange>(context).currentTheme.colorScheme.secondary;
    return Scaffold(
      body: HeaderWave(color: accentColor),
    );
  }
}
