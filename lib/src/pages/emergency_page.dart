import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:diseno_flutter/src/widgets/header_emergency.dart';
import 'package:diseno_flutter/src/widgets/button_widget.dart';

class ItemBoton {
  final IconData icon;
  final String text;
  final Color primaryColor;
  final Color secondColor;

  ItemBoton(this.icon, this.text, this.primaryColor, this.secondColor);
}

class EmergencyPage extends StatelessWidget {
  const EmergencyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final items = <ItemBoton>[
      new ItemBoton(FontAwesomeIcons.carBurst, 'Motor Accident',
          Color(0xff6989F5), Color(0xff906EF5)),
      new ItemBoton(FontAwesomeIcons.plus, 'Medical Emergency',
          Color(0xff66A9F2), Color(0xff536CF6)),
      new ItemBoton(FontAwesomeIcons.themeco, 'Theft / Harrasement',
          Color(0xffF2D572), Color(0xffE06AA3)),
      new ItemBoton(FontAwesomeIcons.bicycle, 'Awards', Color(0xff317183),
          Color(0xff46997D)),
      new ItemBoton(FontAwesomeIcons.carBurst, 'Motor Accident',
          Color(0xff6989F5), Color(0xff906EF5)),
      new ItemBoton(FontAwesomeIcons.plus, 'Medical Emergency',
          Color(0xff66A9F2), Color(0xff536CF6)),
      new ItemBoton(FontAwesomeIcons.tableCells, 'Theft / Harrasement',
          Color(0xffF2D572), Color(0xffE06AA3)),
      new ItemBoton(FontAwesomeIcons.baby, 'Awards', Color(0xff317183),
          Color(0xff46997D)),
      new ItemBoton(FontAwesomeIcons.carBattery, 'Motor Accident',
          Color(0xff6989F5), Color(0xff906EF5)),
      new ItemBoton(FontAwesomeIcons.plus, 'Medical Emergency',
          Color(0xff66A9F2), Color(0xff536CF6)),
      new ItemBoton(FontAwesomeIcons.temperatureArrowDown,
          'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3)),
      new ItemBoton(FontAwesomeIcons.barcode, 'Awards', Color(0xff317183),
          Color(0xff46997D)),
    ];

    List<Widget> itemMap = items
        .map((item) => FadeInLeft(
              duration: const Duration(milliseconds: 500),
              child: ButtonWidget(
                icon: item.icon,
                text: item.text,
                onPress: () {},
                primaryColor: item.primaryColor,
                secondColor: item.secondColor,
              ),
            ))
        .toList();

    return Scaffold(
        body: Stack(children: <Widget>[
      Container(
        margin: const EdgeInsets.only(top: 260),
        child: ListView(
            physics: const BouncingScrollPhysics(),
            children: <Widget>[...itemMap]),
      ),
      _Header(),
    ]));
  }
}

class _Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        HeaderEmergency(
            icon: FontAwesomeIcons.plus,
            title: "Titulo Demo",
            subtitle: "Subtitulo Demo",
            colorSecondary: Colors.blue,
            colorPrimary: Colors.blueAccent),
        Positioned(
            right: -10,
            top: 40,
            child: RawMaterialButton(
                onPressed: () {},
                shape: CircleBorder(),
                padding: EdgeInsets.all(15.0),
                child: FaIcon(FontAwesomeIcons.ellipsisVertical,
                    color: Colors.white)))
      ],
    );
  }
}
