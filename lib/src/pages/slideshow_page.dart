import 'package:diseno_flutter/src/widgets/slide_show.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../theme/themeChange.dart';

class SlideShowPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(child: MySlideShow()),
            Expanded(child: MySlideShow())
          ],
        ),
      ),
    );
  }

}

class MySlideShow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final appTheme = Provider.of<ThemeChange>(context);
    final accentColor = appTheme.currentTheme.colorScheme.secondary;

    return SlideShowWidget(
      pointerUp: false,
      bulletPrimary: 15,
      bulletSecondary: 12,
      colorPrimary: (appTheme.darkTheme) ? accentColor : Color(0xffFF5A7E),
      colorSecondary: Colors.black26,
      slides: <Widget>[
        SvgPicture.asset('assets/svgs/slide-1.svg'),
        SvgPicture.asset('assets/svgs/slide-2.svg'),
        SvgPicture.asset('assets/svgs/slide-3.svg'),
        SvgPicture.asset('assets/svgs/slide-4.svg'),
        SvgPicture.asset('assets/svgs/slide-5.svg'),
      ],
    );
  }
}
