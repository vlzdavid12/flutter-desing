import 'package:diseno_flutter/src/theme/themeChange.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:diseno_flutter/src/widgets/pinterest_medu.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';


class PinterestPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {


    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            PinterestGrid(),
            const MenuLocationAbsolute()
          ],
        )
      ),
    );
  }
}

class MenuLocationAbsolute extends StatelessWidget {
  const MenuLocationAbsolute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final widthScreen = MediaQuery.of(context).size.width;
    final show = Provider.of<_MenuModel>(context).showMenu;
    final appTheme =  Provider.of<ThemeChange>(context).currentTheme;

    return Positioned(
        bottom: 30,
        child: Container(
          width: widthScreen,
          child: Align(
            child: PinterestMenu(
              backgroundColor: appTheme.scaffoldBackgroundColor,
              activeBackgroundColor: appTheme.colorScheme.secondary,
              inactiveBackgroundColor: Colors.white,
              showMenu: show,
              items: [
                PinterestButton(
                    onPressed: () {
                      print('Icon pie_chart');
                    },
                    icon: Icons.pie_chart),
                PinterestButton(
                    onPressed: () {
                      print('Icon search');
                    },
                    icon: Icons.search),
                PinterestButton(
                    onPressed: () {
                      print('Icon notification');
                    },
                    icon: Icons.notifications),
                PinterestButton(
                    onPressed: () {
                      print('Icon supervised_user_circle');
                    },
                    icon: Icons.supervised_user_circle)
              ]
            ),
          ),
        )
    );
  }
}

class PinterestGrid extends StatefulWidget {
  @override
  State<PinterestGrid> createState() => _PinterestGridState();
}

class _PinterestGridState extends State<PinterestGrid> {
  final List<int> items = List.generate(200, (i) => i);
  ScrollController controller =  new ScrollController();
  double scrollBefore = 0;

  @override
  void initState() {
    controller.addListener(() {
     if(controller.offset > scrollBefore && controller.offset > 150){
       Provider.of<_MenuModel>(context, listen: false).showMenu =  false;
     }else{
       Provider.of<_MenuModel>(context, listen: false).showMenu =  true;
     }
     scrollBefore = controller.offset;
    });
    super.initState();
  }


  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MasonryGridView.count(
      controller: controller,
      crossAxisCount: 2,
      mainAxisSpacing: 1,
      crossAxisSpacing: 1,
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) {
        return _PinterestItem(
          index: index,
          extent: (index % 5 + 1) * 100,
        );
      },
    );
  }
}

class _PinterestItem extends StatelessWidget {
  const _PinterestItem({
    Key? key,
    required this.index,
    this.extent,
    this.backgroundColor,
    this.bottomSpace,
  }) : super(key: key);

  final int index;
  final double? extent;
  final double? bottomSpace;
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {

    final child = Container(
      margin: EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.all(Radius.circular(10))
      ),
      height: extent,
      child: Center(
        child: CircleAvatar(
          minRadius: 20,
          maxRadius: 20,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          child: Text('$index', style: const TextStyle(fontSize: 20)),
        ),
      ),
    );

    if (bottomSpace == null) {
      return child;
    }

    return Column(
      children: [
        Expanded(child: child),
        Container(
          height: bottomSpace,
          color: Colors.green,
        )
      ],
    );
  }
}

class _MenuModel  with ChangeNotifier {
  bool _showMenu =  true;
  bool get showMenu => _showMenu;

  set showMenu(bool valor){
    _showMenu = valor;
    notifyListeners();
  }
}
