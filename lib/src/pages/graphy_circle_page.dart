import 'package:diseno_flutter/src/theme/themeChange.dart';
import 'package:diseno_flutter/src/widgets/radial_porgress.dart';
import "package:flutter/material.dart";
import 'package:provider/provider.dart';

class GraphyCirclePage extends StatefulWidget {
  const GraphyCirclePage({Key? key}) : super(key: key);

  @override
  State<GraphyCirclePage> createState() => _GraphyCirclePageState();
}

class _GraphyCirclePageState extends State<GraphyCirclePage> {
  double byPercent =  0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            byPercent += 10;
            if(byPercent > 100) {
              byPercent = 0.0;
            }
          });
        },
        child: Icon(Icons.refresh)
      ),
      body: Center(
        //child: Text('$byPercent %', style: TextStyle(fontSize: 50.0)),
       child: Column(
         mainAxisAlignment: MainAxisAlignment.center,
         children: [
           Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: [
               RadialProgressCircle(byPercent: byPercent,
                   colorPrimary: Colors.red,
                   colorSecondary: Colors.yellow),
               RadialProgressCircle(byPercent: byPercent * 1.2,
                   colorPrimary: Colors.green,
                   colorSecondary: Colors.black12)

             ],
           ),
           Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: [
               RadialProgressCircle(byPercent: byPercent * 1.4,
                   colorPrimary: Colors.purple,
                   colorSecondary: Colors.orange),
               RadialProgressCircle(byPercent: byPercent * 1.6,
                   colorPrimary: Colors.blueAccent,
                   colorSecondary: Colors.redAccent)

             ],
           )
         ],
       ),

      )
    );
  }
}

class RadialProgressCircle extends StatelessWidget {
  const RadialProgressCircle({
    Key? key,
    required this.byPercent,
    required this.colorPrimary,
    required this.colorSecondary
  }) : super(key: key);

  final double byPercent;
  final Color colorPrimary;
  final Color colorSecondary;


  @override
  Widget build(BuildContext context) {
    final appTheme =  Provider.of<ThemeChange>(context).currentTheme;
    return Container(
      height: 140,
      width: 140,
      child: RadialProgress(
          percent: byPercent,
          colorPrimary: colorPrimary,
          colorSecondary: appTheme.textTheme.bodyText1!.color!,
          stroke: 14,
          strokeSecondary: 12),
    );
  }
}


