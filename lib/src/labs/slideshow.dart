import 'package:diseno_flutter/src/models/slider_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class SlideShow extends StatelessWidget {
  const SlideShow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => SliderModel(),
      child: Scaffold(
          body: Center(
              child: Column(
        children: <Widget>[
          Expanded(
            child: _Slides(),
          ),
          _Dots()
        ],
      ))),
    );
  }
}


class _Slides extends StatefulWidget {
  @override
  State<_Slides> createState() => _SlidesState();
}

class _SlidesState extends State<_Slides> {

  final pageViewController =  new PageController();

  @override
  void initState() {
    super.initState();
    pageViewController.addListener(() {
      // print('Page ACTUAL ${pageViewController.page}');
      //Update Provider, SliderModel
      Provider.of<SliderModel>(context, listen: false).currentPage = pageViewController.page!;
    });
  }


  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: pageViewController,
      children: const <Widget>[
        _Slide('assets/svgs/slide-1.svg'),
        _Slide('assets/svgs/slide-2.svg'),
        _Slide('assets/svgs/slide-3.svg'),
        _Slide('assets/svgs/slide-4.svg'),
        _Slide('assets/svgs/slide-5.svg'),
      ],
    );
  }
}

class _Dots extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _Dot(0),
          _Dot(1),
          _Dot(2),
          _Dot(3),
          _Dot(4)
        ],
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;

  _Dot(this.index);

  @override
  Widget build(BuildContext context) {

    final pageViewIndex =  Provider.of<SliderModel>(context).currentPage;

    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      width: 14,
      height: 14,
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration:
          BoxDecoration(
              color: (pageViewIndex >= index - 0.5 && pageViewIndex < index + 0.5)
                  ? Colors.pinkAccent : Colors.grey,
              shape: BoxShape.circle),
    );
  }
}

class _Slide extends StatelessWidget {
  final String svg;

  const _Slide(this.svg);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.all(30),
      child: SvgPicture.asset(svg),
    );
  }
}
