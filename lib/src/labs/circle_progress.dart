import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class CircleProgress extends StatefulWidget {
  const CircleProgress({Key? key}) : super(key: key);

  @override
  State<CircleProgress> createState() => _CircleProgressState();
}

class _CircleProgressState extends State<CircleProgress> with SingleTickerProviderStateMixin {

  late AnimationController controller;
  double byPercent = 0.0;
  double newPercent =  0.0;

  @override
  void initState() {
    controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 800));
    controller.addListener(() {
      //print('Value controller: ${controller.value}');
      setState((){
        byPercent = lerpDouble(byPercent, newPercent, controller.value)!;
      });

    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: (){
          byPercent = newPercent;
          newPercent += 10;
          if(byPercent > 100){
            byPercent = 0;
            newPercent = 0;
          }
          controller.forward(from:  0.0);
          setState(() {});
        },
        backgroundColor: Colors.pink,
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(5),
          width: 300,
          height: 300,
          child: CustomPaint(
            painter: _RadialProgress(byPercent),
          )
        ),
      )
    );
  }
}


class _RadialProgress extends CustomPainter {

  final double byPercent;

  _RadialProgress(this.byPercent);


  // Cicle Complete
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..strokeWidth = 4
      ..color = Colors.grey
      ..style = PaintingStyle.stroke;

    Offset center = Offset(size.width * 0.5, size.height / 2);
    double radio = min(size.width * 0.5, size.height * 0.5);

    canvas.drawCircle(center, radio, paint);

    // Arco
    final paintArco = Paint()
      ..strokeWidth =  10
      ..color =  Colors.pink
      ..style = PaintingStyle.stroke;

    double arcAngle =  2 * pi *  (byPercent / 100);
    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radio),
      -pi / 2,
      arcAngle,
      false,
      paintArco
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;

}
