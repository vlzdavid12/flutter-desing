import 'package:flutter/material.dart';

class HeaderCuadrado extends StatelessWidget {
  const HeaderCuadrado({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(height: 300, color: const Color(0xFF680779));
  }
}

class HeaderRound extends StatelessWidget {
  const HeaderRound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      decoration: const BoxDecoration(
          color: Color(0xFF680779),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
              bottomRight: Radius.circular(50))),
    );
  }
}

class HeaderDiagonal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
            width: double.infinity,
            height: double.infinity,
            child: CustomPaint(
        painter: _HeaderDiagonal() ,
    ),
        );
  }
}

class _HeaderDiagonal extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = const Color(0xFF680779);
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 15;

    final path =  Path();
    path.moveTo(0, size.height * 0.35);
    path.lineTo(size.width, size.height * 0.25);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    path.lineTo(0, size.height * 0.35);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}


class HeaderTriangular extends StatelessWidget {
  const HeaderTriangular({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: CustomPaint(
        painter: _HeaderTriangular() ,
      ),
    );
  }
}

class _HeaderTriangular extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = const Color(0xFF680779);
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 15;

    final path =  Path();
    path.moveTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(0, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}



class HeaderPico extends StatelessWidget {
  const HeaderPico({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: CustomPaint(
        painter: _HeaderPico() ,
      ),
    );
  }
}

class _HeaderPico extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = Color(0xFF680779);
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 15;

    final path =  Path();
    path.lineTo(0, size.height * 0.20);
    path.lineTo(size.width * 0.5, size.height * 0.30);
    path.lineTo(size.width, size.height * 0.20 );
    path.lineTo(size.width, 0);
    path.lineTo(0,0);


    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}


class HeaderCurvo extends StatelessWidget {
  const HeaderCurvo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: CustomPaint(
        painter: _HeaderCurvo() ,
      ),
    );
  }
}

class _HeaderCurvo extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = Color(0xFF680779);
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 15;

    final path =  Path();
    path.lineTo(0, size.height * 0.20);
    path.quadraticBezierTo(size.width * 0.50, size.height * 0.35, size.width, size.height * 0.20);
    path.lineTo(size.width, 0);
    path.lineTo(0,0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}


class HeaderWave extends StatelessWidget {

  final Color color;

  HeaderWave({required this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: CustomPaint(
        painter: _HeaderWavePainter(color: color),
      ),
    );
  }
}

class _HeaderWavePainter extends CustomPainter{
  final Color color;
  _HeaderWavePainter({required this.color});

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = color;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 15;

    final path =  Path();
    path.lineTo(0, size.height * 0.20);
    path.quadraticBezierTo(size.width * 0.20, size.height * 0.25, size.width * 0.50, size.height * 0.20);
    path.quadraticBezierTo(size.width * 0.80, size.height * 0.15, size.width, size.height * 0.20);
    path.lineTo(size.width, 0);
    path.lineTo(0,0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}
