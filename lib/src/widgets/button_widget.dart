import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class ButtonWidget extends StatelessWidget {
  final IconData icon;
  final String text;
  final Color primaryColor;
  final Color secondColor;
  final VoidCallback onPress;

  ButtonWidget({
    required this.icon,
    required this.text,
    this.primaryColor = Colors.deepPurple,
    this.secondColor = Colors.deepPurpleAccent,
    required this.onPress
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Stack(
        children: <Widget>[
          _ButtomBackground(icon: icon, colorPrimary: primaryColor, colorSecondary: secondColor),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(height: 140, width: 40),
              FaIcon(icon, color: Colors.white, size: 40),
              const SizedBox(width: 20),
              Expanded(child: Text(text, style: const TextStyle(color: Colors.white, fontSize: 18))),
              const FaIcon(FontAwesomeIcons.angleRight, color: Colors.white, size: 20),
              const SizedBox(width: 50),
            ],
          )
        ],
      ),
    );
  }
}

class _ButtomBackground extends StatelessWidget {
  final IconData icon;
  final Color colorPrimary;
  final Color colorSecondary;

  const _ButtomBackground({
    Key? key,
    required this.icon,
    required this.colorPrimary,
    required this.colorSecondary,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 100,
      margin: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(color: Colors.black.withOpacity(0.2), offset: const Offset(4,6), blurRadius: 10),
        ],
        borderRadius: BorderRadius.circular(15),
        gradient: LinearGradient(
          colors: <Color>[
            colorPrimary,
            colorSecondary
          ]
        )
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Stack(
          children: <Widget>[
            Positioned(
              right: -20,
              top: -20,
              child: FaIcon(icon, size: 150, color: Colors.white.withOpacity(0.2)),
            )
          ],
        ),
      ),
    );
  }
}
