import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PinterestButton {
  final VoidCallback onPressed;
  final IconData icon;
  PinterestButton({required this.onPressed, required this.icon});
}



class PinterestMenu extends StatelessWidget {
  final bool showMenu;
  final Color backgroundColor;
  final Color activeBackgroundColor;
  final Color inactiveBackgroundColor;
  final List<PinterestButton> items;

  PinterestMenu(
      {this.showMenu = true,
      this.backgroundColor = Colors.white,
      this.activeBackgroundColor = Colors.blueAccent,
      this.inactiveBackgroundColor = Colors.blueGrey,
      required this.items
      });

  /*final List<PinterestButton> items = [
    PinterestButton(
        onPressed: () {
          print('Icon pie_chart');
        },
        icon: Icons.pie_chart),
    PinterestButton(
        onPressed: () {
          print('Icon search');
        },
        icon: Icons.search),
    PinterestButton(
        onPressed: () {
          print('Icon notification');
        },
        icon: Icons.notifications),
    PinterestButton(
        onPressed: () {
          print('Icon supervised_user_circle');
        },
        icon: Icons.supervised_user_circle)
  ];
*/
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => _MenuModel(),
        child: AnimatedOpacity(
          duration: Duration(milliseconds: 250),
          opacity: (showMenu) ? 1 : 0,
          child: Builder(
            builder: (BuildContext context){
              Provider.of<_MenuModel>(context).backgroundColor =  backgroundColor;
              Provider.of<_MenuModel>(context).activeBackgroundColor =  activeBackgroundColor;
              Provider.of<_MenuModel>(context).inactiveBackgroundColor =  inactiveBackgroundColor;
              return _PinterestMenuBackground(child: _MenuItems(items));
            },
          ),
        ));
  }
}



class _PinterestMenuBackground extends StatelessWidget {
  final Widget child;

  _PinterestMenuBackground({required this.child});

  @override
  Widget build(BuildContext context) {
    final bgBox = Provider.of<_MenuModel>(context).backgroundColor;
    return Container(
      child: this.child,
      width: 250,
      height: 60,
      decoration: BoxDecoration(
          color: bgBox,
          borderRadius: BorderRadius.all(Radius.circular(100)),
          boxShadow: <BoxShadow>[
            BoxShadow(color: Colors.black38, blurRadius: 10, spreadRadius: -5)
          ]),
    );
  }
}

class _MenuItems extends StatelessWidget {
  final List<PinterestButton> menuItems;

  _MenuItems(this.menuItems);

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: List.generate(menuItems.length,
            (index) => _PinterestMenuItem(index, menuItems[index])));
  }
}

class _PinterestMenuItem extends StatelessWidget {
  final int index;
  final PinterestButton item;

  _PinterestMenuItem(this.index, this.item);

  @override
  Widget build(BuildContext context) {
    final menuModel = Provider.of<_MenuModel>(context);

    return GestureDetector(
      onTap: () {
        Provider.of<_MenuModel>(context, listen: false).itemSelect = index;
        item.onPressed();
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        child: Icon(
            color: (menuModel.itemSelect == index)
                ? menuModel.activeBackgroundColor
                : menuModel.inactiveBackgroundColor,
            size: (menuModel.itemSelect == index) ? 35 : 28,
            item.icon),
      ),
    );
  }
}

class _MenuModel with ChangeNotifier {
  int _itemSelect = 0;
  Color backgroundColor = Colors.white;
  Color activeBackgroundColor = Colors.blueAccent;
  Color inactiveBackgroundColor = Colors.blueGrey;

  int get itemSelect => _itemSelect;

  set itemSelect(int index) {
    _itemSelect = index;
    notifyListeners();
  }
}
