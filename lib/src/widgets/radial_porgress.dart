import 'dart:math';
import 'dart:ui';
import 'package:flutter/material.dart';

class RadialProgress extends StatefulWidget {
  final percent;
  final Color colorPrimary;
  final Color colorSecondary;
  final double stroke;
  final double strokeSecondary;

  RadialProgress(
      {@required this.percent,
      this.colorPrimary = Colors.blueAccent,
      this.colorSecondary = Colors.grey,
      this.stroke = 10,
      this.strokeSecondary = 10});

  @override
  State<RadialProgress> createState() => _RadialProgressState();
}

class _RadialProgressState extends State<RadialProgress>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  double byPercentPrev = 0.0;

  @override
  void initState() {
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 200));
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    controller.forward(from: 0.0);
    final differentAnimate = widget.percent - byPercentPrev;
    byPercentPrev = widget.percent;

    return AnimatedBuilder(
      animation: controller,
      builder: (BuildContext context, Widget? child) {
        return Container(
          padding: EdgeInsets.all(10),
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            painter: _RadialProgress(
                (widget.percent - differentAnimate) +
                    (differentAnimate * controller.value),
                widget.colorPrimary,
                widget.colorSecondary,
                widget.stroke,
                widget.strokeSecondary),
          ),
        );
      },
    );
  }
}

class _RadialProgress extends CustomPainter {
  final byPercent;
  final Color colorPrimary;
  final Color colorSecondary;
  final double stroke;
  final double strokeSecondary;

  _RadialProgress(this.byPercent, this.colorPrimary, this.colorSecondary,
      this.stroke, this.strokeSecondary);

  // Circle Complete
  @override
  void paint(Canvas canvas, Size size) {
    // Circle Complete
    final paint = Paint()
      ..strokeWidth = stroke
      ..color = colorSecondary
      ..style = PaintingStyle.stroke;

    Offset center = new Offset(size.width * 0.5, size.height / 2);
    double radio = min(size.width * 0.5, size.height * 0.5);

    canvas.drawCircle(center, radio, paint);

    // Arco
    final paintArco = Paint()
      ..strokeWidth = strokeSecondary
      ..color = colorPrimary
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    double arcAngle = 2 * pi * (byPercent / 100);
    canvas.drawArc(Rect.fromCircle(center: center, radius: radio), -pi / 2,
        arcAngle, false, paintArco);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
