import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SlideShowWidget extends StatelessWidget {
  final List<Widget> slides;
  final bool pointerUp;
  final Color colorPrimary;
  final Color colorSecondary;
  final double bulletPrimary;
  final double bulletSecondary;

  SlideShowWidget(
      {required this.slides,
      this.pointerUp = false,
      this.colorPrimary = Colors.blue,
      this.colorSecondary = Colors.grey,
      this.bulletPrimary = 15,
      this.bulletSecondary = 15});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => _SlideShowModel(),
        child: SafeArea(
          child: Center(child: Builder(
            builder: (BuildContext context) {
              Provider.of<_SlideShowModel>(context)._colorPrimary =
                  colorPrimary;
              Provider.of<_SlideShowModel>(context)._colorSecondary =
                  colorSecondary;

              Provider.of<_SlideShowModel>(context)._bulletPrimary =
                  bulletPrimary;
              Provider.of<_SlideShowModel>(context)._bulletSecondary =
                  bulletSecondary;
              return _StructSlideShow(pointerUp, slides);
            },
          )),
        ));
  }
}

class _StructSlideShow extends StatelessWidget {
  final bool pointerUp;
  final List<Widget> slides;

  const _StructSlideShow(this.pointerUp, this.slides);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if (pointerUp) _Dots(slides.length),
        Expanded(
          child: _Slides(slides),
        ),
        if (!pointerUp) _Dots(slides.length),
      ],
    );
  }
}

class _Slides extends StatefulWidget {
  final List<Widget> slides;

  const _Slides(this.slides);

  @override
  State<_Slides> createState() => _SlidesState();
}

class _SlidesState extends State<_Slides> {
  final pageViewController = PageController();

  @override
  void initState() {
    super.initState();
    pageViewController.addListener(() {
      Provider.of<_SlideShowModel>(context, listen: false).currentPage =
          pageViewController.page!;
    });
  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: pageViewController,
      children: widget.slides.map((slide) => _Slide(slide)).toList(),
    );
  }
}

class _Dots extends StatelessWidget {
  final int totalSlides;

  const _Dots(this.totalSlides);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 70,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(totalSlides, (index) => _Dot(index))),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;

  const _Dot(this.index);

  @override
  Widget build(BuildContext context) {
    final slideModel = Provider.of<_SlideShowModel>(context);

    double size = 0;
    Color colorTemp;
    if (slideModel._currentPage >= index - 0.5 &&
        slideModel._currentPage < index + 0.5) {
      size = slideModel.bulletPrimary;
      colorTemp = slideModel._colorPrimary;
    } else {
      size = slideModel.bulletSecondary;
      colorTemp = slideModel._colorSecondary;
    }

    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      width: size,
      height: size,
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
          color: colorTemp,
          shape: BoxShape.circle),
    );
  }
}

class _Slide extends StatelessWidget {
  final Widget svg;

  const _Slide(this.svg);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: const EdgeInsets.all(30),
      child: svg,
    );
  }
}

class _SlideShowModel with ChangeNotifier {
  double _currentPage = 0;
  Color _colorPrimary = Colors.blue;
  Color _colorSecondary = Colors.grey;
  double _bulletPrimary = 0;
  double _bulletSecondary = 0;

  double get currentPage => _currentPage;

  set currentPage(double newPage) {
    _currentPage = newPage;
    notifyListeners();
  }

  Color get colorPrimary => _colorPrimary;

  set colorPrimary(Color color) {
    _colorPrimary = color;
    //notifyListeners();
  }

  Color get colorSecondary => _colorSecondary;

  set colorSecondary(Color color) {
    _colorSecondary = color;
    //notifyListeners();
  }

  double get bulletPrimary => _bulletPrimary;

  set bulletPrimary(double size) {
    _bulletPrimary = size;
    notifyListeners();
  }

  double get bulletSecondary => _bulletSecondary;

  set bulletSecondary(double size) {
    _bulletSecondary = size;
    notifyListeners();
  }
}
