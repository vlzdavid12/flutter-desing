import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class HeaderEmergency extends StatelessWidget {

  final IconData icon;
  final String title;
  final String subtitle;
  final Color colorPrimary;
  final Color colorSecondary;

  HeaderEmergency({
    required this.icon,
    required this.title,
    required this.subtitle,
    this.colorPrimary = Colors.grey,
    this.colorSecondary =  Colors.blueGrey,
});

  @override
  Widget build(BuildContext context) {
    final colorOpacity =  Colors.white.withOpacity(0.5);
    return Stack(
      children: [
        _IconHeaderBackground(colorPrimary: colorPrimary, colorSecondary: colorSecondary),
        Positioned(
            top:-50,
            left: -60,
            child: FaIcon(icon, size: 250, color: Colors.white.withOpacity(0.2))
        ),
        Column(
          children: <Widget>[
            SizedBox(height: 80, width: double.infinity),
            Text(title, style: TextStyle(fontSize:  20, color: colorOpacity)),
            SizedBox(height: 20),
            Text(subtitle, style: TextStyle(fontSize: 25, color: colorOpacity, fontWeight: FontWeight.bold)),
            SizedBox(height: 20),
            FaIcon(icon, size: 80, color: Colors.white)

          ],
        )
      ],
    );
  }
}

class _IconHeaderBackground extends StatelessWidget {

  final Color colorPrimary;
  final Color colorSecondary;

  const _IconHeaderBackground({
    required this.colorPrimary,
    required this.colorSecondary
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(bottomLeft:  Radius.circular(80)),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors:<Color>[colorPrimary, colorSecondary]
        )
    ),

    );
  }
}
