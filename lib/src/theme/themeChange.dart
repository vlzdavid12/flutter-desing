import 'package:flutter/material.dart';

class ThemeChange with ChangeNotifier {
  bool _darkTheme = false;
  bool _customTheme = false;

  late ThemeData _currentTheme;

  bool get darkTheme => _darkTheme;

  bool get customTheme => _customTheme;

  ThemeData get currentTheme => _currentTheme;

  ThemeChange(int theme) {
    switch (theme) {
      case 1: // Light
        _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light();
        break;
      case 2: // Dark
        _darkTheme = true;
        _customTheme = false;
        _currentTheme = ThemeData.dark();
        break;
      case 3: // Custom
        _darkTheme = false;
        _customTheme = true;
        break;
      default:
        _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light();
    }
  }

  set darkTheme(bool value) {
    _customTheme = false;
    _darkTheme = value;
    if (value) {
      _currentTheme = ThemeData.dark();
    } else {
      _currentTheme = ThemeData.light();
    }
    notifyListeners();
  }

  set customTheme(bool value) {
    _customTheme = value;
    _darkTheme = false;
    if(_customTheme) {
      _currentTheme = ThemeData.dark().copyWith(
          colorScheme: ColorScheme(
            brightness: Brightness.dark,
            primary: Colors.pink,
            onPrimary: Colors.pink,
            secondary: Colors.pink,
            onSecondary: Colors.pink,
            error: Colors.deepOrangeAccent,
            onError: Colors.deepOrange,
            background: Colors.indigo,
            onBackground: Colors.indigo,
            surface: Colors.pink,
            onSurface: Colors.white,)
      );
    }
    notifyListeners();
  }
}
