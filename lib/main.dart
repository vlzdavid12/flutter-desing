import 'package:diseno_flutter/src/theme/themeChange.dart';
import 'package:flutter/material.dart';
import 'package:diseno_flutter/src/pages/launcher_page.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
      ChangeNotifierProvider(
          create: (_) => ThemeChange(1),
          child: MyApp()
      )
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final currentTheme =  Provider.of<ThemeChange>(context).currentTheme;
    return OrientationBuilder(
      builder: (BuildContext context, Orientation orientation) {
        return Container(
            child: MaterialApp(
              theme: currentTheme,
              debugShowCheckedModeBanner: false,
              title: 'Flutter Demo',
              home: const LauncherPage(),
            ),
        );
      },
    );
  }
}


